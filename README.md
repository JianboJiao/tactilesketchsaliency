# Tactile Sketch Saliency

By [Jianbo Jiao](https://jianbojiao.com/), [Ying Cao](http://ying-cao.com/), [Manfred Lau](https://sites.google.com/site/manfredlau/), [Rynson W. H. Lau](http://www.cs.cityu.edu.hk/~rynson/).


### Introduction

This repository contains the dataset and implementations for the paper "[Tactile Sketch Saliency](https://jianbojiao.com/pdfs/ACMMM20.pdf)". In this work, we aim to understand the functionality of 2D sketches by predicting how humans would interact with the objects depicted by sketches in real life. Given a 2D sketch, we learn to predict atactile saliency map for it, which represents where humans would interact with (e.g. grasp, press, or touch) the object depicted by the sketch. Examples are illustrated in the figure below:

![teaser](imgs/teas.png)

### Dataset

To address the problem, we construct a new dataset consisting of 2D sketches and their corresponding tactile saliency maps, depth maps and the semantic categories. Examples are presented in the figure below and more details please refer to the paper:

![data](imgs/data.png)

To generate depth map $D_{iv}$, we use ray casting. In particular, we create a 2D grid of the same resolution as the depth map, where each grid cell contains a list of faces from $M_i$ whose 2D projections overlap with the grid cell. We initially go through each face of $M_i$ once and put it in the grid cells that it projects to. For each pixel, we then perform ray casting and compute ray-triangle intersections with only the faces in the corresponding grid cell. To generate 2D tactile saliency map $S_{iv}$, we take the saliency value at each ray intersection point. These saliency data are in a vertex-wise form, with a saliency value for each vertex in $M_i$. However, as a ray can intersect the model at anywhere on its surface, for each ray intersection point, we take the corresponding face and compute the barycentric-weighted combination of the vertex saliency values of the face (obtained from the existing 3D tactile data) to be the saliency value at the intersection point. To generate 2D sketch $I_{iv}$, we take the depth map $D_{iv}$ and perform Sobel edge detection to generate an outline of the projected 3D shape. While there are more complex methods to generate sketches, we find that this method is sufficient for our purpose.
The corresponding data can be downloaded [here](https://drive.google.com/file/d/1UkV4WsXvYxcFJhGBEoqpOOHitmQ58qn0/view?usp=sharing), in which three subfolders *sketch*, *saliency* and *depth* store the corresponding images. Please store the downloaded data in the folder "[data](./data)".

In addition, for the evaluation of real data, we collect user-study data from Amazon Mechanical Turk (AMT). An example on how this was performed is shown below. In total, we perform 10 Human Intelligence Tasks (HITs) on AMT. The corresponding collected data can be downloaded [here](https://drive.google.com/file/d/13JeiqabTmblsk7Ux5UDvQ8yrMtX9fRZc/view?usp=sharing) and please store the downloaded data in folder "[AMT](./AMT)". The real sketch images with those randomly selected point pairs (A and B) are stored in each folder named with its category (e.g. mug). For each HIT result file (e.g. sketch_points_AorB_HIT01_9users.txt), the user-study result is stored according to different categories. Under each category, there are 5 rows representing the 5 point pairs. The first column is the image id/name, the following 4 columns are the corresponding coordinates of the two points, the last 9 columns are the choices (labeled results, A or B) from the AMT users.

![AMT](imgs/AMT.gif)

### Implementation

0. This proposed approach was implemented with the [MatConvNet](https://www.vlfeat.org/matconvnet/) framework on Linux with GPUs. Please setup the environment according to the [instructions](https://www.vlfeat.org/matconvnet/install/) first.
1. An example implementation is shown in file "[train.m](train.m)" to train the model.
2. The results will be stored in folder "out_logs".

### Citation

If you find this work useful or use our dataset or code, please consider citing:

	@InProceedings{Jiao_MM20,
		author = {Jiao, Jianbo and Cao, Ying and Lau, Manfred and Lau, Rynson},
		title = {Tactile Sketch Saliency},
		year = {2020},
		booktitle = {Proceedings of the 28th ACM International Conference on Multimedia},
	}
	
Please email the [authors](mailto:jiaojianbo.i@gmail.com or jianbo@robots.ox.ac.uk) if you have any questions.