function y = getBatch(imdb, images, varargin)
% GET_BATCH  Load, preprocess, and pack images for CNN evaluation

opts.imageSize = [200, 200];
opts.numAugments = 1 ;
opts.transformation = 'none' ;
opts.rgbMean = [] ;
opts.rgbVariance = zeros(0,1,'single') ;
opts.labelStride = 1 ;
opts.labelOffset = 0 ;
opts.interpolation = 'bilinear' ;
opts.numThreads = 1 ;
opts.prefetch = false ;
opts.useGpu = false ;
opts = vl_argparse(opts, varargin);

if opts.prefetch
  % to be implemented
  ims = [] ;
  labels = [] ;
  return ;
end

% space for images
ims = zeros(opts.imageSize(1), opts.imageSize(2), 1, ...
  numel(images)*opts.numAugments, 'single') ;
% space for labels
lx = opts.labelOffset : opts.labelStride : opts.imageSize(2) ;
ly = opts.labelOffset : opts.labelStride : opts.imageSize(1) ;
labels = zeros(numel(ly), numel(lx), 1, numel(images)*opts.numAugments, 'single') ;
depths = zeros(numel(ly), numel(lx), 1, numel(images)*opts.numAugments, 'single') ;
classes=zeros(1,numel(images)*opts.numAugments);

WH = 192;
RS = 200-WH+1;
ims = zeros(WH,WH,1,numel(images),'single'); 
labels = zeros(WH,WH,1,numel(images),'single'); 
depths = zeros(WH,WH,1,numel(images),'single'); 

im = cell(1,numel(images)) ;
si = 1 ;

for i=1:numel(images)
  % acquire image
  if isempty(im{i})
    rgbPath = sprintf(imdb.paths.image, imdb.images.name{images(i)}) ;
    labelsPath = sprintf(imdb.paths.gt, imdb.images.name{images(i)}) ;
    depthPath = sprintf(imdb.paths.depth, imdb.images.name{images(i)}) ;
    rgb = imread(rgbPath) ;
    anno = imread(labelsPath) ;
    depth=imread(depthPath);
    class=imdb.images.class(images(i));
    rgb=im2single(rgb);
    anno=single(anno);
    depth=single(depth);
  else
    rgb = im{i} ;
  end

  % crop & flip
  h = size(rgb,1) ;
  w = size(rgb,2) ;
  for ai = 1:opts.numAugments
    x = randperm(RS,1);
    y = randperm(RS,1);
    roll = rand();
    if roll>0.45
        degree=randi([-10 10],1,1);
        tmpImg = imrotate(rgb,degree,'bilinear','crop');
        tmpSal = imrotate(anno,degree,'bilinear','crop');
        tmpDep = imrotate(depth,degree,'bilinear','crop');
    else
        tmpImg = rgb;
        tmpSal=anno;
        tmpDep=depth;
    end
    ims(:,:,:,si) = tmpImg(x:(x+WH-1),y:(y+WH-1),:);
    labels(:,:,1,si) = tmpSal(x:(x+WH-1),y:(y+WH-1),:);
    depths(:,:,1,si) = tmpDep(x:(x+WH-1),y:(y+WH-1),:);
    roll = rand();
    if roll>0.5
        ims(:,:,:,si) = fliplr(ims(:,:,:,si));
        labels(:,:,:,si) = fliplr(labels(:,:,:,si));
        depths(:,:,:,si) = fliplr(depths(:,:,:,si));
    end
    
    classes(1,si)=single(class);
    
    si = si + 1 ;
  end
end
if opts.useGpu
  ims = gpuArray(ims) ;
  labels=gpuArray(labels);
  depths=gpuArray(depths);
  classes=gpuArray(classes);
end
y = {'input', ims, 'label', labels,'depth',depths,'class',classes} ;
