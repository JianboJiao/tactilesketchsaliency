function train(varargin)

run ../../../matconvnet-1.0-beta25/matlab/vl_setupnn;
addpath '../../../matconvnet-1.0-beta25/examples/';

% experiment and data paths
opts.expDir = 'out_logs/' ;
opts.imgDir='data/sketch/train';
opts.gtDir='data/saliency/train';
opts.dpDir='data/depth/train';

[opts, varargin] = vl_argparse(opts, varargin) ;

% experiment setup
opts.imdbPath = fullfile(opts.expDir, 'imdb.mat') ;
opts.imdbStatsPath = fullfile(opts.expDir, 'imdbStats.mat') ;

% training options (SGD)
opts.train = struct([]) ;
[opts, varargin] = vl_argparse(opts, varargin) ;

trainOpts.batchSize = 1 ;
trainOpts.continue = true ;
trainOpts.gpus = [1] ;
trainOpts.prefetch = true ;
trainOpts.expDir = opts.expDir ;
trainOpts.learningRate = [1e-5 * ones(1,20),1e-6 * ones(1,20),1e-7 * ones(1,460)] ;
trainOpts.numEpochs = numel(trainOpts.learningRate) ;
% -------------------------------------------------------------------------
% Setup data
% -------------------------------------------------------------------------
if exist(opts.imdbPath)
  imdb = load(opts.imdbPath) ;
else
    imdb = DataSetup('imgDir', opts.imgDir, ...
      'gtDir', opts.gtDir, ...
      'dpDir', opts.dpDir, ...
    'includeTest', false) ;
  mkdir(opts.expDir) ;
  save(opts.imdbPath, '-struct', 'imdb') ;
end
% Get training and test/validation subsets
train = find(imdb.images.set == 1) ;
val = find(imdb.images.set == 2) ;
% -------------------------------------------------------------------------
% Setup model
% -------------------------------------------------------------------------
net=InitModel();

% -------------------------------------------------------------------------
% Train
% -------------------------------------------------------------------------
% Setup data fetching options
opts.train=trainOpts;
bopts.labelStride = 1 ;
bopts.labelOffset = 1 ;
bopts.useGpu = numel(opts.train.gpus) > 0 ;
bopts.imageSize=[192 192];
multi_loss=1;
if multi_loss==1
    % opts.train.derOutputs = {'objective', 1, 'objective2',1, 'objective3', 0.5} ;
    opts.train.derOutputs = {'objective', 0.4, 'objective2',0.3, 'objective3', 0.3} ;
else
    opts.train.derOutputs = {'objective', 1} ;
end
% Launch SGD
info = cnn_train_dag(net, imdb, getBatchWrapper(bopts), ...
                     trainOpts, ....
                     'train', train, ...
                     'val', val, ...
                     opts.train) ;

% -------------------------------------------------------------------------
function fn = getBatchWrapper(opts)
% -------------------------------------------------------------------------
fn = @(imdb,batch) getBatch(imdb,batch,opts,'prefetch',nargout==0) ;
