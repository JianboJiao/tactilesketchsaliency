function imdb = DataSetup(varargin)

opts.imgDir='';
opts.gtDir='';
opts.dpDir='';
opts.includeTest = false ;
opts = vl_argparse(opts, varargin); 

% Source images and classes
imdb.paths.image = esc(fullfile(opts.imgDir, '%s.png')) ;
imdb.paths.gt = esc(fullfile(opts.gtDir, '%s.png')) ;
imdb.paths.depth = esc(fullfile(opts.dpDir, '%s.png')) ;
imdb.sets.id = uint8([1 2 3]) ;
imdb.sets.name = {'train', 'val', 'test'} ;
imdb.images.name = {} ;
imdb.images.set = [] ;
imdb.images.class=[];
%watch,002_spoon,marker,mug,cooking,shove,desk,wine,screw,sedan, fork,sugarbowl,creamer,teapot,backpack,basket,bottleketch,bottlespray,handbag,stat1,stat2,stat3,stat4,stat5,stat6=[1..25]

img_path=opts.imgDir;
imgPath=[];
imgPath=cat(1,imgPath,dir(fullfile(img_path,'*.png')));
num=length(imgPath);
p=randperm(num);
for i=1:floor(0.95*num)
    name=imgPath(p(i)).name(1:end-4);
    imdb.images.name{i}=name;
    imdb.images.set(i)=1;
    if strcmp(name(1:3),'wat')
        imdb.images.class(i)=1;
    elseif strcmp(name(5:8),'spoo')
        imdb.images.class(i)=2;
    elseif strcmp(name(1:3),'mar')
        imdb.images.class(i)=3;
    elseif strcmp(name(1:3),'mug')
        imdb.images.class(i)=4;
    elseif strcmp(name(1:3),'coo')
        imdb.images.class(i)=5;
    elseif strcmp(name(1:3),'sho')
        imdb.images.class(i)=6;
    elseif strcmp(name(1:3),'des')
        imdb.images.class(i)=7;
    elseif strcmp(name(1:3),'win')
        imdb.images.class(i)=8;
    elseif strcmp(name(1:3),'scr')
        imdb.images.class(i)=9;
    elseif strcmp(name(1:3),'sed')
        imdb.images.class(i)=10;
    elseif strcmp(name(5:8),'fork')
        imdb.images.class(i)=11;
    elseif strcmp(name(5:8),'suga')
        imdb.images.class(i)=12;
    elseif strcmp(name(5:8),'crea')
        imdb.images.class(i)=13;
    elseif strcmp(name(5:8),'teap')
        imdb.images.class(i)=14;
    elseif strcmp(name(1:3),'bac')
        imdb.images.class(i)=15;
    elseif strcmp(name(1:3),'bas')
        imdb.images.class(i)=16;
    elseif strcmp(name(7:9),'ket')
        imdb.images.class(i)=17;
    elseif strcmp(name(7:9),'spr')
        imdb.images.class(i)=18;
    elseif strcmp(name(1:3),'han')
        imdb.images.class(i)=19;
    elseif strcmp(name(8:9),'01')
        imdb.images.class(i)=20;
    elseif strcmp(name(8:9),'02')
        imdb.images.class(i)=20;
    elseif strcmp(name(8:9),'me')
        imdb.images.class(i)=20;
    elseif strcmp(name(8:9),'03')
        imdb.images.class(i)=21;
    elseif strcmp(name(8:9),'04')
        imdb.images.class(i)=22;
    elseif strcmp(name(8:9),'06')
        imdb.images.class(i)=23;
    elseif strcmp(name(8:9),'07')
        imdb.images.class(i)=24;
    elseif strcmp(name(8:9),'08')
        imdb.images.class(i)=25;
    end
end
for i=floor(0.95*num)+1:num
    name=imgPath(p(i)).name(1:end-4);
    imdb.images.name{i}=name;
    imdb.images.set(i)=2;
    if strcmp(name(1:3),'wat')
        imdb.images.class(i)=1;
    elseif strcmp(name(1:3),'002')
        imdb.images.class(i)=2;
    elseif strcmp(name(1:3),'mar')
        imdb.images.class(i)=3;
    elseif strcmp(name(1:3),'mug')
        imdb.images.class(i)=4;
    elseif strcmp(name(1:3),'coo')
        imdb.images.class(i)=5;
    elseif strcmp(name(1:3),'sho')
        imdb.images.class(i)=6;
    elseif strcmp(name(1:3),'des')
        imdb.images.class(i)=7;
    elseif strcmp(name(1:3),'win')
        imdb.images.class(i)=8;
    elseif strcmp(name(1:3),'scr')
        imdb.images.class(i)=9;
    elseif strcmp(name(1:3),'sed')
        imdb.images.class(i)=10;
    elseif strcmp(name(5:8),'fork')
        imdb.images.class(i)=11;
    elseif strcmp(name(5:8),'suga')
        imdb.images.class(i)=12;
    elseif strcmp(name(5:8),'crea')
        imdb.images.class(i)=13;
    elseif strcmp(name(5:8),'teap')
        imdb.images.class(i)=14;
    elseif strcmp(name(1:3),'bac')
        imdb.images.class(i)=15;
    elseif strcmp(name(1:3),'bas')
        imdb.images.class(i)=16;
    elseif strcmp(name(7:9),'ket')
        imdb.images.class(i)=17;
    elseif strcmp(name(7:9),'spr')
        imdb.images.class(i)=18;
    elseif strcmp(name(1:3),'han')
        imdb.images.class(i)=19;
    elseif strcmp(name(8:9),'01')
        imdb.images.class(i)=20;
    elseif strcmp(name(8:9),'02')
        imdb.images.class(i)=20;
    elseif strcmp(name(8:9),'me')
        imdb.images.class(i)=20;
    elseif strcmp(name(8:9),'03')
        imdb.images.class(i)=21;
    elseif strcmp(name(8:9),'04')
        imdb.images.class(i)=22;
    elseif strcmp(name(8:9),'06')
        imdb.images.class(i)=23;
    elseif strcmp(name(8:9),'07')
        imdb.images.class(i)=24;
    elseif strcmp(name(8:9),'08')
        imdb.images.class(i)=25;
    end
end

% Compress data types
imdb.images.set = uint8(imdb.images.set) ;

% Check images on disk and get their size
imdb = getImageSizes(imdb) ;

% -------------------------------------------------------------------------
function imdb = getImageSizes(imdb)
% -------------------------------------------------------------------------
for j=1:numel(imdb.images.name)
  info = imfinfo(sprintf(imdb.paths.image, imdb.images.name{j})) ;
  imdb.images.size(:,j) = uint16([info.Width ; info.Height]) ;
  fprintf('%s: checked image %s [%d x %d]\n', mfilename, imdb.images.name{j}, info.Height, info.Width) ;
end
% -------------------------------------------------------------------------
function str=esc(str)
% -------------------------------------------------------------------------
str = strrep(str, '\', '\\') ;